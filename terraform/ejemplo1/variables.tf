# archivo donde declararemos variables
variable "nombre_archivo" {
    type = string
    description = "describe el nombre del archivo a crear"
}

variable "archivos" {
    type = list(string)
    description = "lista de archivos que se van a definir" 
    default = [ 
        
    ]
}