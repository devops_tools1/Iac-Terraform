# archivo main de terraform donde declararemos los recursos

resource "local_file" "archivo1" {
    content     = "este es archivo1, editado"
    filename = "/home/${var.nombre_archivo}"
}

resource "local_file" "files" {
  for_each = toset(var.archivos)
  content = "contenido: ${each.key}"
  filename = "/home/${each.key}"
}