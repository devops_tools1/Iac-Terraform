# configuracion de variables

variable "identificador" {
    type        =  string
    description = "identificador para el bucket de s3"
}

variable "servidores" {
    type        = list(object({
        nombre  = string
        tipo    = string
        ami     = string
    }))

    description = "servidores que se crearan en la cuenta de aws"
}