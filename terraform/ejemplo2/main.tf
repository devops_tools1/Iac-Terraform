# configuracion main

locals {
  env_prefix = "${var.identificador}-${terraform.workspace}"
  servers_keys = var.servidores.*.nombre
  server_vals  = var.servidores.*
  server_map   = zipmap(local.servers_keys, local.server_vals)
}

resource "aws_instance" "servers" {
  for_each      = toset(local.servers_keys)
  ami           = lookup(local.server_map, each.key).ami
  instance_type = lookup(local.server_map, each.key).tipo

  tags = {
    Name = "${local.env_prefix}-${each.key}"
    Env  = terraform.workspace
  }
}