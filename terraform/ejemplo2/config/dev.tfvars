identificador = "terraform"

servidores = [
    {
        nombre = "ubuntu20.04"
        tipo   = "t2.medium"
        ami    = "ami-09e67e426f25ce0d7"
    },
    {
        nombre = "amazonlinux2"
        tipo   = "t2.medium"
        ami    = "ami-02e136e904f3da870"
    },
    {
        nombre = "debian10"
        tipo   = "t2.medium"
        ami    = "ami-07d02ee1eeb0c996c"
    }
]