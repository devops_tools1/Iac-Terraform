# Iac Terraform

## Sobre el proyecto

> A continuación se presentan ejemplos básicos para el uso de terraform

## Antes de empezar

<p style='text-align: justify;'> Para los siguientes ejemplos estan pensados para ser probados utilizando un contenedor de Docker, por lo que se provee el dockerfile necesario para poder instalar dependencias y todo lo que se necesita para poder correr y probar cada uno de los ejemplo. Si quieres mayor informacion de como instalar docker en tu computadora local puedes dirigirte al siguiente enlace: </p>

* [Instalacion de Docker](https://docs.docker.com/desktop/)

+ [Creadenciales de AWS](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)

### Como correr los ejemplos

<p style='text-align: justify;'> 
Para poder correr los ejemplos, este repositorio cuenta con un Dockerfile que permite crear el contenedor con el ambiente ya preparado para poder probar los ejemplos. Para ejecutarlos puedes seguir los siguientes pasos
</p>

1. Construye el contenedor de Docker

```bash
$ docker build -t <NOMBRE_CONTENEDOR>:<TAG_CONTENEDOR> docker/
```

2. Corre el contenedor y monta el directorio de **terraform** en el contenedor

```bash
$ docker run -it --rm -v ~/.aws:/root/.aws -v $PWD/terraform:/root/terraform <NOMBRE_CONTENEDOR>:<TAG_CONTENEDOR>
```

**NOTA**: Es importante saber como crear credenciales para la cuenta de AWS, ya que se esta montando el directorio **~/.aws** de la maquina local a el contendor, esto para poder utilizar credenciales dentro del contendor al desplegar utilizando terraform.

3. Ahora te encontraras en el contenedor y ya puedes probar los ejemplos:

```bash
➜  ~ terraform -version
Terraform v1.0.9
on linux_amd64
```

## Ejemplos

### Ejemplo 1

<p style='text-align: justify;'>
En el ejemplo 1 podemos ver una estructura básica de un proyecto de terraform. Se utiliza el recurso de terraform local_file para poder mostrar una funcionalidad básica de terraform. En el ejemplo 1 se tratará lo siguiente:
</p>

* Estructura de un proyecto de terraform (básico)
* Uso del archivo de variables
* Pasar variables a un modulo de terraform utilizando la bandera **-var-file**
* Uso del commando **plan**
* Uso del comando **apply**
* Uso del comando **destroy**

### Ejemplo 2

<p style='text-align: justify;'>
En el ejemplo 2 se utilizará la configuración y proveedor de AWS para poder crear recursos en una cuenta de AWS. Se utilizará el recurso de aws_instance para poder crear de forma dinámica servidores. El ejemplo contiene las siguiente información
</p>

* Uso de el comando **workspace**
* Creación de workspaces para manejar múltiples ambientes
* Listar los workspaces
* Cambiar de workspaces
* Crear recursos en la nube de AWS
* Crear recursos en diferentes workspaces
* Manejo de funciones en terraform (lookup, zipmap)
* Creación dinamica de recursos con terraform
* Uso del commando **show** para mostrar el estado de la infraestructura.


## Documentación de referencia:

* [Lenguaje de Terraform](https://www.terraform.io/docs/language/index.html)
* [AWS provider para Terraform](https://registry.terraform.io/providers/hashicorp/aws/latest)

## Youtube Playlist

* [Playlist de video ejemplos](https://youtube.com/playlist?list=PL8Xnw9lMxPCFOH2iDpG6SQdqonfIB0ntu)
